const promise = require('bluebird');
const options = {
    promiseLib: promise
};
const pgp = require('pg-promise')(options);
const settings = require('./settings');
const db = pgp(settings.getSettings());

console.log("DB created");

function getAllGyms(success, failure) {
    const query = 'SELECT * FROM info.club ORDER BY id ASC;';
    db.any(query)
        .then(function resolve(data) {
            console.log("Successfylly fetched all gyms");
            success(data);
        })
        .catch(function error(err) {
            console.log("An error occured while fetching all gyms: " + JSON.stringify(err));
            failure(err);
        });
}

module.exports = {
    getAllGyms: getAllGyms
};