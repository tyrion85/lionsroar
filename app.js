'use strict'
const express = require('express');
const app = express();

const db = require('./repository/repository');

app.get('/gyms', function (request, response) {
    console.log("GET gyms endpoint called.");
    const result = db.getAllGyms(function(data) {
        response.status(200).send(data);
    }, function(err) {
        response.status(500).send(err);
    });
});

module.exports = app;

